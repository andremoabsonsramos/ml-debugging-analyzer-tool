square = lambda x, y, z : x*y*z

def get_value(x):
    return x

print(square(get_value(10), get_value(get_value(20)), get_value(10)))
print(get_value(10))