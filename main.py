import ast
import copy
from pprint import pprint
from alembic.command import current

graph = {}

# print(graph)

class Analyzer(ast.NodeVisitor):
    def __init__(self):
        self.report = {"import": [], "importFrom": []}
        self.calls = []
        self.stack = []
        self.args = []
        self.empilhando = False
        self.idx = 0
        self.depth = -1
        self.parent = None
        self.depthParent = {}
        
        self.lastCallIdx = None
        self.lastCallDepth = None
        
    def get_additional_info(self, node):
        additional_info = {
                "lineno" : node.lineno,
                "col_offset" : node.col_offset
            } 
        
        return additional_info

    def visit_Import(self, node):
        additional_info = self.get_additional_info(node)
        
        for child in node.names:            
            name = str(child.name) + ":" + str(child.asname)
            
            self.report["import"].append((name, additional_info))
             
        self.generic_visit(node)
        
    def visit_ImportFrom(self, node):        
        tmp = {"from" : str, "import" : []}
        
        tmp["from"] = node.module
        
        additional_info = self.get_additional_info(node)
        
        for child in node.names:
            name = str(child.name) + ":" + str(child.asname)
            
            tmp["import"].append(name)
            
        self.report["importFrom"].append((tmp, additional_info))
        
        self.generic_visit(node)
        
    def processArg(self, pos, arg, arg_name=None):
        out = {
            "pos" : pos,
            "arg_name" : arg_name,
            "value" : None,
            "lineno" : arg.lineno,
            "col_offset" : arg.col_offset
        }
        
        if isinstance(arg, ast.Num):
            out["value"] = arg.n
        elif isinstance(arg, ast.Str):
            out["value"] = arg.s
        elif isinstance(arg, ast.Name):
            out["value"] = arg.id
        elif isinstance(arg, ast.List):
            out["value"] = "ast.List"
        elif isinstance(arg, ast.Call):
            out["value"] = "ast.Call"
        else:
            print(arg)
            
        return out

    
    def visit_Call(self, node):
        if self.depth == -1:
            self.parent = None
            print("ROOT", self.depth * ' ', "idx:", -1, "depth:", self.depth)
        
        self.depth += 1

        self.empilhando = True
        
        pos = 0
        for arg in node.args:
            self.args.append(self.processArg(pos, arg));

            pos += 1
            
        for keyword in node.keywords:
            arg_name = keyword.arg
            arg = keyword.value
            self.args.append(self.processArg(pos, arg, arg_name))
            
            pos += 1
            
        if (self.lastCallDepth != self.depth):
            self.lastCallIdx = self.parent
        
        print((self.depth + 1) * ' ', "idx:", self.idx, "depth:", self.depth, "parent:", self.lastCallIdx, "lastCallIdx:", self.lastCallIdx, "lastCallDepth:", self.lastCallDepth, ast.dump(node))
        
        self.lastCallDepth = self.depth
        self.parent = self.idx
        
        self.generic_visit(node)
      
        self.depth -= 1
        
    def visit_Attribute(self, node):
        if (self.empilhando):
            self.stack.append(node.attr)
         
        self.generic_visit(node)
         
    def visit_Name(self, node):
        if self.empilhando:
            self.stack.append(node.id)
             
            call = ".".join(reversed(self.stack))
            additional_info = self.get_additional_info(node)
                        
            self.calls.append((self.idx, call, additional_info, copy.deepcopy(self.args)))
            
            self.stack.clear()
            self.empilhando = False
            
#             print(self.args)
            self.args.clear()
     
            self.idx += 1
            
         
        self.generic_visit(node)

    def print_report(self):
        print("[ IMPORTS ]")
        pprint(self.report)
        print()
        print("[ CALLS ]")
        pprint(self.calls)

        
with open("exemplo2.py", "r") as file:
    tree = ast.parse(file.read())

    analyzer = Analyzer()
    analyzer.visit(tree)
#     analyzer.print_report()