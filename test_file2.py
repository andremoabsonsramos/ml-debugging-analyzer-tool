from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.model_selection import KFold, cross_val_predict, cross_val_score
from sklearn.metrics import classification_report, f1_score, make_scorer
import numpy as np
import pandas as pd

def knn():    
    data = pd.read_csv('data/iris.data')
    
    x = data.iloc[:,:4]
    y = data.iloc[:,-1]
    
    model = KNeighborsClassifier(n_neighbors = 3)
    model.fit(x, y)
    
    test = pd.DataFrame([[1.4, 3.6, 3.4, 1.2]])
    
    print(model)
    print(model.predict(test))
    
    kfold = KFold(n_splits = 10, random_state = 7)
    y_pred = cross_val_predict(KNeighborsClassifier(n_neighbors = 3), x, y, cv = kfold)
    
    scores = cross_val_score(KNeighborsClassifier(n_neighbors = 3), x, y, cv = 10, scoring=make_scorer(f1_score, average='macro'))
    print(np.mean(scores))

    print(classification_report(y, y_pred))

def naive_bayes():
    data = pd.read_csv('data/iris.data')
     
#     data.type.replace(['Iris-setosa', 'Iris-versicolor', 'Iris-virginica'], [1, 2, 3], inplace = True)
#     print(data.values)
     
    x = data.iloc[:,:4]
    y = data.iloc[:,-1]
     
    model = GaussianNB()
     
    model.fit(x, y)
     
    test = pd.DataFrame([[1.4, 3.6, 3.4, 1.2]])
     
    print(model)
    print(model.predict(test))
     
    kfold = KFold(n_splits = 10, random_state = 7)
    
    y_pred = cross_val_predict(GaussianNB(), x, y, cv = kfold)
    
    scores = cross_val_score(GaussianNB(), x, y, cv = 10, scoring=make_scorer(f1_score, average='macro'))
    print(np.mean(scores))
    
    print(classification_report(y, y_pred))


knn()
print()
naive_bayes()